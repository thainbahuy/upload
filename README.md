# Upload large files about > 500mb in php
Js library: plupload
## Getting started
Config nginx
````
{
    client_max_body_size 51M;
    listen 8074;
    server_name localhost;
    root /usr/local/var/www/upload;
    index index.html index.php;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }
    error_page 500 502 503 504 /50x.html;
    location = /50x.html {
        root html;
    }
    location ~ \.php$ {
        fastcgi_pass 127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
}
````