<?php
//ini_set('display_errors', '1');
//ini_set('display_startup_errors', '1');
//error_reporting(E_ALL);
//if (isset($_FILES["file"])) {
////    $source = $_FILES['up']['tmp_name'];
////    $destination = dirname(__FILE__). "/file_upload/{$_FILES['up']['name']}";
////    var_dump(move_uploaded_file($source, $destination));
//    var_dump($_POST,$_REQUEST);
////    if (empty($_FILES) || $_FILES['file']['error']) {
////        die(json_encode(['error' => 'Failed to move uploaded file.']));
////    }
//}

// Make sure file is not cached (as it happens for example on iOS devices)
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

// Settings
$targetDir = 'file_upload';
$cleanupTargetDir = true; // Remove old files
$maxFileAge = 5 * 3600; // Temp file age in seconds

// Create target dir
if (!file_exists($targetDir)) {
    @mkdir($targetDir);
}

// Get a file name
if (isset($_REQUEST["name"])) {
    $fileName = $_REQUEST["name"];
} elseif (!empty($_FILES)) {
    $fileName = $_FILES["file"]["name"];
} else {
    $fileName = uniqid("file_");
}

//$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;
$filePath = __DIR__ . DIRECTORY_SEPARATOR . $targetDir;
if (!file_exists($filePath)) {
    if (!mkdir($filePath, 0777, true)) {
        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to create $filePath!"}}');
    }
}
$fileName = $_REQUEST["name"] ?? $_FILES["file"]["name"];
$filePath = $filePath . DIRECTORY_SEPARATOR . $fileName;
// Chunking might be enabled
$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

// Remove old temp files
if ($cleanupTargetDir) {
    if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
        die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
    }

    while (($file = readdir($dir)) !== false) {
        $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

        // If temp file is current file proceed to the next
        if ($tmpfilePath == "{$filePath}.part") {
            continue;
        }

        // Remove temp file if it is older than the max age and is not the current file
        if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
            @unlink($tmpfilePath);
        }
    }
    closedir($dir);
}

$out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
if ($out) {
    $in = @fopen($_FILES['file']['tmp_name'], "rb");
    if ($in) {
        while ($buff = fread($in, 4096)) { fwrite($out, $buff); }
    } else {
        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream!"}}');
    }
    @fclose($in);
    @fclose($out);
    @unlink($_FILES['file']['tmp_name']);
} else {
    die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open output stream!"}}');
}
if (!$chunks || $chunk == $chunks - 1) {
    rename("{$filePath}.part", $filePath);
}
die('{"jsonrpc" : "2.0", "result" : {"status": 200, "message": "The file has been uploaded successfully!"}}');
//?>